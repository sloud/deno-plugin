install_deno() {
    local whereami="$1"
    local version="$2"
    local url="https://deno.land/x/install/install.sh"
    local bin="$HOME/.deno/bin"
    local completions_dir="$whereami/src"
    
    path+=($bin)

    if ! type deno 2>&1 >/dev/null; then
        mkdir -p "$bin"
        curl -fsSL "$url" >"$whereami/install-deno.sh"
        bash "$whereami/install-deno.sh"

        mkdir -p "$completions_dir"
        "$bin/deno" completions zsh >"$completions_dir/_deno"
    fi
    
    fpath+=("$completions_dir")
}

install_deno "${0:A:h}"
unset install_deno